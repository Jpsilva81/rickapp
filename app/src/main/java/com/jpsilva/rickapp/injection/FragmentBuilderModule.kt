package com.jpsilva.rickapp.injection

import com.jpsilva.rickapp.ui.fragment.CharactersDetailFragment
import com.jpsilva.rickapp.ui.fragment.CharactersListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {
    @ContributesAndroidInjector
    abstract fun contributeCharacterListFragment(): CharactersListFragment

    @ContributesAndroidInjector
    abstract fun contributeCharacterDetailFragment(): CharactersDetailFragment
}