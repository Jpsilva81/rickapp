package com.jpsilva.rickapp.injection

import com.jpsilva.rickapp.api.NetworkModule
import com.jpsilva.rickapp.ui.viewmodel.CharacterDetailsViewModel
import com.jpsilva.rickapp.ui.viewmodel.CharacterListItemViewModel
import com.jpsilva.rickapp.ui.viewmodel.CharactersListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    fun inject(characterListItemViewModel: CharacterListItemViewModel)

    fun inject(characterDetailsViewModel: CharacterDetailsViewModel)

    fun inject(charactersListViewModel: CharactersListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector
        fun networkModule(networkModule: NetworkModule): Builder
    }
}