package com.jpsilva.rickapp.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.jpsilva.rickapp.R
import com.jpsilva.rickapp.databinding.CharacterItemBinding
import com.jpsilva.rickapp.model.RickResult.Character
import com.jpsilva.rickapp.ui.viewmodel.CharacterListItemViewModel

class CharacterListAdapter: RecyclerView.Adapter<CharacterListAdapter.ViewHolder>() {
    private lateinit var itemsList:List<Character>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: CharacterItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.character_item, parent, false)
        return ViewHolder(binding).listen { pos, _ ->
            val item = itemsList[pos]
            val bundle = bundleOf("id" to item.id.toString())
            parent.findNavController().navigate(R.id.action_to_detail_fragment, bundle)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemsList[position])
    }

    override fun getItemCount(): Int {
        return if(::itemsList.isInitialized) itemsList.size else 0
    }

    fun updateDataList(itemsList:List<Character>){
        this.itemsList = itemsList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: CharacterItemBinding):RecyclerView.ViewHolder(binding.root){
        private val viewModel = CharacterListItemViewModel()

        fun bind(item:Character){
            viewModel.bind(item)
            binding.viewModel = viewModel
        }
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(adapterPosition, itemViewType)
        }
        return this
    }

}