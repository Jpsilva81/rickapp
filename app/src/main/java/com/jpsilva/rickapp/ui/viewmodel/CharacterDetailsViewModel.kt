package com.jpsilva.rickapp.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.jpsilva.rickapp.api.RickApi
import com.jpsilva.rickapp.base.BaseViewModel
import com.jpsilva.rickapp.model.RickResult.Character
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CharacterDetailsViewModel : BaseViewModel(){

    @Inject
    lateinit var rickApi: RickApi

    private lateinit var subscription: Disposable

    private val characterName = MutableLiveData<String>()
    private val characterImage = MutableLiveData<String>()
    private val characterGender = MutableLiveData<String>()
    private val characterEpisodes = MutableLiveData<String>()
    private val characterLocationName = MutableLiveData<String>()
    private val characterType = MutableLiveData<String>()

    private fun bind(item: Character){
        characterName.value = item.name
        characterImage.value = item.image
        characterGender.value = item.gender
        characterEpisodes.value = item.episode.count().toString()
        characterLocationName.value = item.location.name
        characterType.value = item.type
    }

    fun loadData(id: String){
        subscription = rickApi.getCharacterById(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrieveDataListStart() }
            .doOnTerminate { onRetrieveDataListFinish() }
            .subscribe(
                { result -> onRetrieveDataListSuccess(result)  },
                { error -> onRetrieveDataListError(error) }
            )
    }

    private fun onRetrieveDataListStart(){

    }

    private fun onRetrieveDataListFinish(){

    }

    private fun onRetrieveDataListSuccess(result : Character){
        bind(result)
    }

    private fun onRetrieveDataListError(error: Throwable){
        Log.d("Error", error.toString())
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
    fun getCharacterName(): MutableLiveData<String> {
        return characterName
    }

    fun getCharacterImage(): MutableLiveData<String> {
        return characterImage
    }

    fun getCharacterGender(): MutableLiveData<String> {
        return characterGender
    }
    fun getCharacterEpisodes(): MutableLiveData<String> {
        return characterEpisodes
    }
    fun getCharacterLocationName(): MutableLiveData<String>{
        return characterLocationName
    }
    fun getCharacterType(): MutableLiveData<String>{
        return characterType
    }
}