package com.jpsilva.rickapp.ui.viewmodel


import android.util.Log
import android.view.View.*
import androidx.lifecycle.MutableLiveData
import com.jpsilva.rickapp.api.RickApi
import com.jpsilva.rickapp.base.BaseViewModel
import com.jpsilva.rickapp.model.RickResult
import com.jpsilva.rickapp.ui.adapter.CharacterListAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CharactersListViewModel : BaseViewModel(){

    @Inject
    lateinit var rickApi: RickApi

    private lateinit var subscription: Disposable
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val resultListAdapter: CharacterListAdapter =
        CharacterListAdapter()

    fun loadData(){
        subscription = rickApi.getCharacters(1)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrieveDataListStart() }
                .doOnTerminate { onRetrieveDataListFinish() }
                .subscribe(
                        { result -> onRetrieveDataListSuccess(result)  },
                        { error -> onRetrieveDataListError(error) }
                )
    }

    private fun onRetrieveDataListStart(){
        loadingVisibility.value = VISIBLE
    }

    private fun onRetrieveDataListFinish(){
        loadingVisibility.value = GONE
    }

    private fun onRetrieveDataListSuccess(result : RickResult){
        resultListAdapter.updateDataList(result.results)
    }

    private fun onRetrieveDataListError(error: Throwable){
        Log.d("Error", error.toString())
    }

}