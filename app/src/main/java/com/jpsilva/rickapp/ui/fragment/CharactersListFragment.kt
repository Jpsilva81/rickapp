package com.jpsilva.rickapp.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.jpsilva.rickapp.R
import com.jpsilva.rickapp.databinding.FragmentCharacterListBinding
import com.jpsilva.rickapp.injection.Injectable
import com.jpsilva.rickapp.ui.viewmodel.CharactersListViewModel
import kotlinx.android.synthetic.main.fragment_character_list.*


class CharactersListFragment : Fragment(), Injectable {

    private lateinit var binding: FragmentCharacterListBinding
    lateinit var viewModel: CharactersListViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentCharacterListBinding>(
                inflater, R.layout.fragment_character_list, container, false).apply {
            lifecycleOwner = this@CharactersListFragment
        }
        binding.resultsList.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        viewModel = ViewModelProviders.of(this).get(CharactersListViewModel::class.java)
        viewModel.loadData()
        binding.viewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                return false
            }

        })


    }


}