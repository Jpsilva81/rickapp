package com.jpsilva.rickapp.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.jpsilva.rickapp.R
import com.jpsilva.rickapp.databinding.FragmentCharacterDetailBinding
import com.jpsilva.rickapp.injection.Injectable
import com.jpsilva.rickapp.ui.viewmodel.CharacterDetailsViewModel

class CharactersDetailFragment : Fragment(), Injectable {

    private lateinit var binding: FragmentCharacterDetailBinding
    private lateinit var viewModel: CharacterDetailsViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentCharacterDetailBinding>(
                inflater, R.layout.fragment_character_detail, container, false).apply {
            lifecycleOwner = this@CharactersDetailFragment
        }

        viewModel = ViewModelProviders.of(this).get(CharacterDetailsViewModel::class.java)
        arguments?.getString("id")?.let { viewModel.loadData(it) }
        binding.viewModel = viewModel
        return binding.root
    }

}
