package com.jpsilva.rickapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import com.jpsilva.rickapp.base.BaseViewModel
import com.jpsilva.rickapp.model.RickResult.Character

class CharacterListItemViewModel : BaseViewModel() {

    private val characterName = MutableLiveData<String>()
    private val characterImage = MutableLiveData<String>()

    fun bind(item: Character){
        characterName.value = item.name
        characterImage.value = item.image
    }

    fun getCharacterName():MutableLiveData<String>{
        return characterName
    }

    fun getCharacterImage():MutableLiveData<String>{
        return characterImage
    }
}