package com.jpsilva.rickapp.base

import androidx.lifecycle.ViewModel
import com.jpsilva.rickapp.api.NetworkModule
import com.jpsilva.rickapp.injection.DaggerViewModelInjector
import com.jpsilva.rickapp.injection.ViewModelInjector
import com.jpsilva.rickapp.ui.viewmodel.CharacterDetailsViewModel
import com.jpsilva.rickapp.ui.viewmodel.CharacterListItemViewModel
import com.jpsilva.rickapp.ui.viewmodel.CharactersListViewModel
import io.reactivex.disposables.Disposable

abstract class BaseViewModel: ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector.builder()
        .networkModule(NetworkModule)
        .build()

    private lateinit var subscription: Disposable

    init {
        inject()
    }

    private fun inject() {
        when (this) {
            is CharactersListViewModel -> injector.inject(this)
            is CharacterDetailsViewModel -> injector.inject(this)
            is CharacterListItemViewModel -> injector.inject(this)

        }
    }

}