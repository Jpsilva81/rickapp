package com.jpsilva.rickapp.api

import com.jpsilva.rickapp.model.RickResult
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RickApi {

    @GET("api/character/")
    fun getCharacters(@Query("page") page: Int): Observable<RickResult>

    @GET("api/character/{id}")
    fun getCharacterById(@Path("id") id: String): Observable<RickResult.Character>

}